﻿using Domain.Types;
using System;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var latitude = new Latitude(85);
            Console.WriteLine(latitude.ToString());

            var version = new FirmwareVersion(1, 0, 0);
            Console.WriteLine(version.ToString());
        }
    }
}
