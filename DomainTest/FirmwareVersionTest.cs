﻿using Domain.Types;
using NFluent;
using System;
using Xunit;

namespace DomainTest
{
    public class FirmwareVersionTest
    {
        [Fact]
        public void Can_indicate_version()
        {
            var target = new FirmwareVersion(1,0,0);
            Check.That(target.ToString()).IsEqualTo("v.1.0.0");
        }

        [Theory]
        [InlineData(1, 0, 1)]
        [InlineData(49, 99, 99)]
        [InlineData(0, 0, 0)]
        public void Can_create_version_with_this_particular_values(byte g, byte r, byte c)
        {
            var target = new FirmwareVersion(g, r, c);
            Check.That(target).IsNotNull();
        }

        [Theory]
        [InlineData(100, 0, 1)]
        [InlineData(1, 100, 100)]
        public void Should_throw_exception_if_version_is_not_valid(byte g, byte r, byte c)
        {
            Check.ThatCode(() => new FirmwareVersion(g, r, c)).Throws<ArgumentException>();
        }
    }
}
