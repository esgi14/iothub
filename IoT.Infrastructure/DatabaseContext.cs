﻿using IoT.Application.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace IoT.Infrastructure
{
    public class DatabaseContext : DbContext
    {
        public DbSet<SensorModel> Sensors { get; set; }
        public DbSet<StationModel> Stations { get; set; }
        public DbSet<UserModel> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql(@"Host=51.161.69.30;Database=testdb;Username=esgi;Password=8NnMJZnC");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StationModel>(entity =>
            {
                entity.ToTable("Station");
            });

            modelBuilder.Entity<SensorModel>(entity =>
            {
                entity.ToTable("Sensor");
                entity.OwnsMany(sensorModel => sensorModel.Data, _ =>
                {
                    _.ToTable("Data");
                    _.HasKey("Id");
                    _.Property("CreatedAt").ValueGeneratedOnAddOrUpdate().HasDefaultValue(DateTime.Now);
                });
            });

            modelBuilder.Entity<UserModel>(entity =>
            {
                entity.ToTable("User");
            });
        }
    }
}
