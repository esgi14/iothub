﻿using Domain.Commons;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class User : Entity
    {
        private readonly List<Station> _stations = new List<Station>();



        public string FirstName { get;  }

        public string LastName { get; }

        public DateTime DateOfBirth { get;  }

        public string Company { get; set; }

        public User(string firstName, string lastname, DateTime dateOfBirth)
        {
            FirstName = firstName;
            LastName = lastname;
            DateOfBirth = dateOfBirth;
        }

        public IEnumerable<Station> Stations => _stations;


        public void Add(Station station)
        {
            _stations.Add(station);
        }
    }
}
