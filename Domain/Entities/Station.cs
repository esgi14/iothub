﻿using Domain.Commons;
using Domain.Types;
using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class Station : Entity
    {
        private readonly List<Sensor> _sensors = new List<Sensor>();

        public string Name { get; set; }

        public string City { get; set; }

        public Latitude Latitude { get; set; }

        public Longitude Longitude { get; set; }

        public bool IsActive { get; set; }

        public DateTime InstallationDate { get; set; }

        public DateTime OperatingStateDateTime { get; set; }

        public DateTime UpdateDateTime { get; set; }

        public FirmwareVersion Version { get; set; }

        public IEnumerable<Sensor> Sensors => _sensors;

        public void Add(Sensor sensor)
        {
            _sensors.Add(sensor);
        }
    }
}
