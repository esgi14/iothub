﻿using Domain.Commons;
using Domain.ValueObjects;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class Sensor : Entity
    {
        private readonly List<Data> _data = new List<Data>();
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public string Type { get; set; }

        public IEnumerable<Data> Data => _data;

        public void Add(Data data)
        {
            _data.Add(data);
        }
    }
}
