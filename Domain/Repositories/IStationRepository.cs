﻿using Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface IStationRepository
    {
        Task<List<Station>> GetListAsync();

        Task<Station> GetById(int stationId);

        Task<int> InsertAsync(Station station);

        Task UpdateAsync(Station station);

        Task DeleteAsync(Station station);
    }
}
