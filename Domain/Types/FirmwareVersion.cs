﻿using System;

namespace Domain.Types
{
    public class FirmwareVersion
    {
        public string Value { get; set; }

        /// <summary>
        /// Une version de firmware comporte 3 informations précisant la génération, la révision et la correction.
        /// Le numéro de génération doit être entre 0 et 50.
        /// Le numéro de révision doit être entre 0 et 99.
        /// le numéro de correction doit être entre 0 et 99.
        /// </summary>
        /// <param name="generation">indicateur de génération</param>
        /// <param name="revision">indicateur de révision</param>
        /// <param name="correction">indicateur de correction</param>
        public FirmwareVersion(byte generation, byte revision, byte correction)
        {
            if (generation > 50 || revision > 99 || correction > 99)
                throw new ArgumentException("Invalid version value : {Value} ");


            Value = $"{generation}.{revision}.{correction}";
        }

        public override string ToString() => $"v.{Value}";
    }
}
