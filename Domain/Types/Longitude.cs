﻿using System;

namespace Domain.Types
{
    /// <summary>
    /// Classe définissant un tye Longitude compris entre -180 et 180°
    /// </summary>
    public class Longitude
    {
        public float Value { get; }

        /// <summary>
        /// Une longitude est comprise entre -180 et 180°
        /// </summary>
        /// <param name="value">angle de longitude</param>
        public Longitude(float value)
        {
            if (value < -180.00 || value > 180.00)
                throw new ArgumentException("Invalid longitude");

            Value = value;
        }
        public override string ToString() => $"{Value}°";
    }
}
