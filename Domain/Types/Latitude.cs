﻿using System;

namespace Domain.Types
{
    /// <summary>
    /// Classe définissant une Latitude comprise entre -90° et 90°
    /// </summary>
    public class Latitude
    {
        public float Value { get;  }

        /// <summary>
        /// Une latitude est comprise entre -90 et 90°
        /// </summary>
        /// <param name="value">angle de latitude</param>
        public Latitude(float value)
        {
            if (value < -90.00 || value > 90.00)
                throw new ArgumentException("Value out of boundary");
            Value = value;
        }

        public override string ToString() => $"{Value}°";
    }
}
