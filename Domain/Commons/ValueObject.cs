﻿using System.Collections.Generic;
using System.Linq;

namespace Domain.Commons
{
    public abstract class ValueObject
    {
        /// <summary>
        /// Méthode de comparaison de value Object
        /// </summary>
        /// <param name="left">Value Object source</param>
        /// <param name="right">Value Object cible</param>
        /// <returns>True ou false</returns>
        protected static bool EqualOperator(ValueObject left, ValueObject right)
        {
            if (ReferenceEquals(left, null) ^ ReferenceEquals(right, null))
            {
                return false;
            }
            return ReferenceEquals(left, null) || left.Equals(right);
        }

        /// <summary>
        /// Méthode permettant de vérifier que deux value object sont bien différents
        /// </summary>
        /// <param name="left">value object source</param>
        /// <param name="right">value object cible</param>
        /// <returns>true ou false</returns>
        protected static bool NotEqualOperator(ValueObject left, ValueObject right)
        {
            return !(EqualOperator(left, right));
        }

        /// <summary>
        /// Retourne les éléments semblables de chaque value object
        /// </summary>
        /// <returns>Liste d'éléments identiques</returns>
        protected abstract IEnumerable<object> GetEqualityComponents();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var other = (ValueObject)obj;

            return this.GetEqualityComponents().SequenceEqual(other.GetEqualityComponents());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return GetEqualityComponents()
                .Select(x => x != null ? x.GetHashCode() : 0)
                .Aggregate((x, y) => x ^ y);
        }

        // Other utility methods
    }
}
