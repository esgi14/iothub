﻿using Domain.Commons;
using System.Collections.Generic;

namespace Domain.ValueObjects
{
    public class Data : ValueObject
    {
        public float Value { get; set; }



        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
        }
    }
}
