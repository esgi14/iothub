# Projet avec approche en Clean Architecture

## Description 
Projet en C#, .Net Core 3.1 permettant d'appréhender la Clean Architecture. 
Son Architecture est la suivante : 
1. Core/
 *  Application
 *  Domain
2. Infrastructure
3. Presentation

### Core/Domain
La couche centrale définit les * Data Rules*, toutes nos données et les typages forts que nous avons créer pour renforcer la cohérence de la donnée.

### Core/Application 
La couche qui vient par dessus la couche * Domain * et qui contient nos * Business Rules *, les règles qui vont définir comment nous allons intéragir avec notre donnée. Ces règles sont définis dans des "services" qui s'appuient sur les * Repositories * qui seront définis dans notre couche de persistence. 

### Infrastructure
Cette couche contient la liaison physique avec notre persistence, ici une base de données SQL en s'appuyant sur les deux couches définit dans * Core * .

### Presentation 
Cette couche contient tous les projets permettant l'interaction Homme Machine avec notre logiciel. 

