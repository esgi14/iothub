﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.Application.Models
{
    public class BaseModel
    {
        public Guid Id { get; set; }
    }
}
