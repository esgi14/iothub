﻿
namespace IoT.Application.Models
{
    public class SensorModel : BaseModel
    {
        public bool IsActive { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public virtual IList<DataModel> Data { get; set; }

        public virtual StationModel Station { get; set; }
    }
}
