﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.Application.Models
{
    public class DataModel
    {
        public float Value { get; set; }

        public DateTime CreatedAt { get; set; }

        public virtual SensorModel Sensor { get; set; }
    }
}
