﻿using System;

namespace IoT.Application.Models
{
    public class StationModel : BaseModel
    {
        public string City { get; set; }

        public bool IsActive { get; set; }

        public DateTime InstallationDateTime { get; set; }

        public float Latitude { get; set; }

        public float Longitude { get; set; }

        public string Name { get; set; }

        public DateTime OperatingStateDateTime { get; set; }

        public DateTime UpdateDateTime { get; set; }

        public string Version { get; set; }

        public virtual IList<SensorModel> Sensors { get; set; }

        public virtual UserModel User { get; set; }
    }
}
