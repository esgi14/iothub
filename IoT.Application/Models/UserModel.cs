﻿using System;
using System.Collections.Generic;

namespace IoT.Application.Models
{
    public class UserModel : BaseModel
    {
        public string Company { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public virtual IList<StationModel> Stations { get; set; }
    }
}
