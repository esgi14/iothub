﻿using IoT.Application.Models;

namespace IoT.Application.Interfaces
{
    public interface ISensorService : IService<SensorModel>
    {
    }
}
