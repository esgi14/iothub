﻿using IoT.Application.Models;

namespace IoT.Application.Interfaces
{
    public interface IUserService : IService<UserModel>
    {
    }
}
