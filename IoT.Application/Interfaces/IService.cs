﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace IoT.Application.Interfaces
{
    public interface IService<T>
    {
        Task<int> CreateAsync(T applicationModel);

        Task Delete(int modelId);

        Task<List<T>> GetListAsync();

        Task<T> GetById(int modelId);

        Task UpdateAsync(T applicationModel);
    }
}
