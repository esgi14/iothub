﻿using IoT.Application.Models;

namespace IoT.Application.Interfaces
{
    public interface IStationService : IService<StationModel>
    {
    }
}
