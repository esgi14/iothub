﻿using Domain.Entities;
using IoT.Application.Interfaces;
using IoT.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Types;
using Domain.Repositories;

namespace IoT.Application.Services
{
    public class StationService : IStationService
    {

        private readonly IStationRepository _stationRepository;



        public StationService(IStationRepository stationRepository)
        {
            _stationRepository = stationRepository;
        }



        public async Task<int> CreateAsync(StationApplicationModel applicationModel)
        {
            if (applicationModel == null)
                throw new ArgumentNullException(nameof(CreateAsync));

            var stationToBeCreated = new Station()
            {
                Name = applicationModel.Name,
                Latitude = new Latitude(applicationModel.Latitude),
                Longitude = new Longitude(applicationModel.Longitude),
                // etc
            };

            int stationId = await _stationRepository.InsertAsync(stationToBeCreated);
            return stationId;
        }

        public async Task Delete(int modelId)
        {
            try
            {
                var stationToDelete = await _stationRepository.GetById(modelId);
                if (stationToDelete == null)
                    throw  new Exception($"No station has been found with this id {modelId}");

                await _stationRepository.DeleteAsync(stationToDelete);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<StationApplicationModel> GetById(int modelId)
        {
            var result =  await _stationRepository.GetById(modelId);
            var stationApplicationModel = new StationApplicationModel()
            {
                City = result.City,
                IsActive = result.IsActive,
                Latitude = result.Latitude.Value,
                Longitude = result.Longitude.Value,
                Name = result.Name,
                Version = result.Version.Value
                // etc
            };
            return stationApplicationModel;
        }

        public async Task<List<StationModel>> GetListAsync()
        {
            var stations = await _stationRepository.GetListAsync();
            var list = new List<StationModel>();
            foreach (var station in stations)
            {
                var stationApplicationModel = new StationModel()
                {
                    Name = station.Name,
                    // etc 
                })
                .ToList();
        }

        public Task UpdateAsync(StationModel applicationModel)
        {
            // TODO : trouver la station, mais il me faut son ID et pour l'instant, je n'ai pas cette propriété dans mon model
            
            throw new NotImplementedException();
        }
    }
}
